# README #

* Aplicação desenvolvida utilizando Java 15 e SpringBoot
* O banco de dados escolhido é um H2, não havendo a necessidade de instalação de bancos externos, porém é um banco volátil. 
* Para acesso ao banco utilizar: localhost:9878/h2 usuario sa sem senha e o banco comp2
* A aplicação fica disponível na porta 9878
* Foi inserido o swagger para documentação dos end-points, para acessa-lo basta utilizar o caminho: localhost:9878/swagger-ui.html
* Existem alguns testes unitários utilizando MockMvc
