package com.ueda2.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ueda2.api.controller.ContatoController;
import com.ueda2.api.entity.Contato;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ContatoTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ContatoController contatoController;

    @Test
    void InserindoContato() throws Exception {
        Contato contato = new Contato();
        contato.setNome("Aline Vanin");
        contato.setTelefone("46991310403");
        contato.setEmail("aline@gmail.com");

        mockMvc.perform(post("/contato")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(contato)))
                .andExpect(status().isCreated());
    }

    @Test
    void InserindoContatoSemEmail() throws Exception {
        Contato contato = new Contato();
        contato.setNome("Aline Vanin");
        contato.setTelefone("46991310403");

        mockMvc.perform(post("/contato")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(contato)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void InserindoContatoSemTelefone() throws Exception {
        Contato contato = new Contato();
        contato.setNome("Aline Vanin");
        contato.setEmail("aline@gmail.com");

        mockMvc.perform(post("/contato")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(contato)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void InserindoContatoSemNome() throws Exception {
        Contato contato = new Contato();
        contato.setEmail("aline@gmail.com");
        contato.setTelefone("46991310403");

        mockMvc.perform(post("/contato")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(contato)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void ConsultandoContatoExiste() throws Exception {

        mockMvc.perform(get("/contato/1"))
                .andExpect(status().isOk());
    }

    @Test
    void DeletandoContatoExiste() throws Exception {

        mockMvc.perform(delete("/pessoa/2"))
                .andExpect(status().isOk());
    }

    @Test
    void ConsultandoContatoNaoExiste() throws Exception {

        mockMvc.perform(get("/contato/150"))
                .andExpect(status().isNoContent());
    }

    @Test
    void DeletandoContatoNaoExiste() throws Exception {

        mockMvc.perform(delete("/pessoa/150"))
                .andExpect(status().isBadRequest());
    }
}
