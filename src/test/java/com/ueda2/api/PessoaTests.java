package com.ueda2.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ueda2.api.controller.PessoaController;
import com.ueda2.api.entity.Pessoa;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PessoaTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PessoaController pessoaController;

    @Test
    void InserindoPessoa() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Daniel Ueda");
        pessoa.setCpf("21788217071");
        pessoa.setDataNascimento(LocalDate.parse("1994-04-03"));

        mockMvc.perform(post("/pessoa")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(pessoa)))
                .andExpect(status().isCreated());
    }

    @Test
    void InserindoPessoaSemCPF() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Daniel Ueda");
        pessoa.setDataNascimento(LocalDate.parse("1994-04-03"));

        mockMvc.perform(post("/pessoa")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(pessoa)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void InserindoPessoaSemDataNascimento() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Daniel Ueda");
        pessoa.setCpf("21788217071");

        mockMvc.perform(post("/pessoa")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(pessoa)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void InserindoPessoaSemNome() throws Exception {
        Pessoa pessoa = new Pessoa();
        pessoa.setCpf("21788217071");
        pessoa.setDataNascimento(LocalDate.parse("1994-04-03"));

        mockMvc.perform(post("/pessoa")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(pessoa)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void ConsultandoPessoaExiste() throws Exception {

        mockMvc.perform(get("/pessoa/1"))
                .andExpect(status().isOk());
    }

    @Test
    void DeletandoPessoaExiste() throws Exception {

        mockMvc.perform(delete("/pessoa/2"))
                .andExpect(status().isOk());
    }

    @Test
    void DeletandoPessoaNaoExiste() throws Exception {

        mockMvc.perform(delete("/pessoa/150"))
                .andExpect(status().isBadRequest());
    }


    @Test
    void ConsultandoPessoaNaoExiste() throws Exception {

        mockMvc.perform(get("/pessoa/5"))
                .andExpect(status().isNoContent());
    }

    @Test
    void ConsultandoListaPessoa() throws Exception {

        mockMvc.perform(get("/pessoa/"))
                .andExpect(status().isOk());
    }

    @Test
    void VinculandoContatoPessoa() throws Exception {

        mockMvc.perform(post("/pessoa/1/contato/1/add"))
                .andExpect(status().isOk());
    }

    @Test
    void VinculandoContatoNaoExistePessoa() throws Exception {

        mockMvc.perform(post("/pessoa/1/contato/3/add"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void VinculandoContatoPessoaNaoExiste() throws Exception {

        mockMvc.perform(post("/pessoa/4/contato/1/add"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void DesvinculandoContatoPessoa() throws Exception {

        mockMvc.perform(post("/pessoa/1/contato/1/delete"))
                .andExpect(status().isOk());
    }

    @Test
    void DesinculandoContatoNaoExistePessoa() throws Exception {

        mockMvc.perform(post("/pessoa/1/contato/3/delete"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void DesvinculandoContatoPessoaNaoExiste() throws Exception {

        mockMvc.perform(post("/pessoa/4/contato/1/delete"))
                .andExpect(status().isBadRequest());
    }
}
