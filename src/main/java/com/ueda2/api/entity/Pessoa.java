package com.ueda2.api.entity;

import lombok.*;
import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome", length = 500)
    @NotNull(message = "Nome não pode ser nulo")
    private String nome;

    @Column(name = "cpf", length = 11)
    @NotNull(message = "CPF não pode ser nulo")
    @CPF(message = "CPF não válido")
    private String cpf;

    @Column(name="datanascimento")
    @Past
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Data Nascimento não pode ser nula")
    private LocalDate dataNascimento;

    @OneToMany
    @JoinColumn(name = "pessoa_id")
    private List<Contato> contatos = new ArrayList<>();

    public void addContato(Contato contato){
        contatos.add(contato);
    }

    public void removeContato(Contato contato){
        contatos.remove(contato);
    }

}