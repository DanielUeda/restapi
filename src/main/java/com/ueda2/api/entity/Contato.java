package com.ueda2.api.entity;


import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Contato implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome", length = 500)
    @NotNull(message = "Nome não pode ser nulo")
    private String nome;

    @Column(name = "telefone", length = 50)
    @NotNull(message = "Telefone não pode ser nulo")
    private String telefone;

    @Column(name = "email", length = 100)
    @NotNull(message = "Email não pode ser nulo")
    @Email(message = "Email não válido")
    private String email;

}
