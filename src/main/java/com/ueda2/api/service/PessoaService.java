package com.ueda2.api.service;

import com.ueda2.api.entity.Pessoa;
import com.ueda2.api.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class PessoaService {

    @Autowired
    PessoaRepository repository;

    public Page<Pessoa> search( String searchTerm, int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "nome");

        return repository.search( searchTerm.toLowerCase(), pageRequest);
    }
}
