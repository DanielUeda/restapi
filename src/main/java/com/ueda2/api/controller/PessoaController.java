package com.ueda2.api.controller;

import com.ueda2.api.service.PessoaService;
import com.ueda2.api.entity.Contato;
import com.ueda2.api.entity.Pessoa;
import com.ueda2.api.repository.ContatoRepository;
import com.ueda2.api.repository.PessoaRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Autowired
    PessoaService service;

    @GetMapping("")
    public ResponseEntity<?> list() {

        List<Pessoa> pessoaList = pessoaRepository.findAll();

        if (pessoaList.size() == 0){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Sem dados!");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(pessoaList);
        }

    }

    @ApiOperation(value = "Visualiza uma pessoa")
    @GetMapping("/{codigo}")
    public ResponseEntity<?> visualizar(@PathVariable Long codigo) {
        Optional<Pessoa> pessoa = pessoaRepository.findById(codigo);

        if (pessoa.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Sem dados!");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(pessoa.get());
        }
    }

    @ApiOperation(value = "Retorna uma lista paginada filtrando por nome ou cpf")
    @GetMapping("/search")
    public Page<Pessoa> search(
            @RequestParam("searchTerm") String searchTerm,
            @RequestParam(
                    value = "page",
                    required = false,
                    defaultValue = "0") int page,
            @RequestParam(
                    value = "size",
                    required = false,
                    defaultValue = "10") int size) {
        return service.search(searchTerm, page, size);

    }

    @ApiOperation(value = "Salva uma pessoa")
    @PostMapping("")
    public ResponseEntity<?> save(@RequestBody @Valid Pessoa pessoa) {
        try{
            pessoaRepository.save(pessoa);
            return ResponseEntity.status(HttpStatus.CREATED).body("Pessoa cadastrada com sucesso!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

    }

    @ApiOperation(value = "Deleta uma pessoa")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try{
            pessoaRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body("Pessoa deletada com sucesso!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @ApiOperation(value = "Vincula um contato à uma pessoa")
    @PostMapping(value = "{pessoaId}/contato/{contatoId}/add")
    public ResponseEntity<?> addContato(@PathVariable final Long pessoaId, @PathVariable final Long contatoId){
        try{
            Pessoa pessoa = pessoaRepository.findById(pessoaId).get();
            Contato contato = contatoRepository.findById(contatoId).get();
            pessoa.addContato(contato);
            pessoaRepository.save(pessoa);
            return ResponseEntity.status(HttpStatus.OK).body("Contato vinculado com sucesso!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @ApiOperation(value = "Atualiza uma pessoa")
    @PutMapping("/{id}")
    public ResponseEntity<?> atualizaPessoa(@PathVariable Long id, @RequestBody @Valid Pessoa pessoa) {
        try{
            Optional<Pessoa> pessoaOriginal = pessoaRepository.findById(id);
            if (pessoaOriginal.isPresent()) {
                pessoaOriginal.get().setNome(pessoa.getNome());
                pessoaOriginal.get().setDataNascimento(pessoa.getDataNascimento());
                pessoaOriginal.get().setCpf(pessoa.getCpf());
                pessoaRepository.save(pessoaOriginal.get());
                return ResponseEntity.status(HttpStatus.OK).body("Pessoa atualizada com sucesso!");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Pessoa não encontrada para atualização!");
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @ApiOperation(value = "Desvincula um contato de uma pessoa")
    @PostMapping(value = "{pessoaId}/contato/{contatoId}/delete")
    public ResponseEntity<?> deleteContato(@PathVariable final Long pessoaId, @PathVariable final Long contatoId){
        try{
            Pessoa pessoa = pessoaRepository.findById(pessoaId).get();
            Contato contato = contatoRepository.findById(contatoId).get();
            pessoa.removeContato(contato);
            pessoaRepository.save(pessoa);
            return ResponseEntity.status(HttpStatus.OK).body("Contato desvinculado com sucesso!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
