package com.ueda2.api.controller;

import com.ueda2.api.entity.Contato;
import com.ueda2.api.entity.Pessoa;
import com.ueda2.api.repository.ContatoRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    private ContatoRepository contatoRepository;

    @ApiOperation(value = "Visualiza um contato")
    @GetMapping("/{codigo}")
    public ResponseEntity<?> visualizar(@PathVariable Long codigo) {
        Optional<Contato> contato = contatoRepository.findById(codigo);

        if (contato.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body("Sem dados!");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(contato.get());
        }
    }

    @ApiOperation(value = "Salva um contato")
    @PostMapping("")
    public ResponseEntity<?> save(@RequestBody @Valid Contato contato) {
        try{
            contatoRepository.save(contato);
            return ResponseEntity.status(HttpStatus.CREATED).body("Contato cadastrado com sucesso!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }

    }

    @ApiOperation(value = "Atualiza um contato")
    @PutMapping("/{id}")
    public ResponseEntity<?> atualizaContato(@PathVariable Long id, @RequestBody @Valid Contato contato) {
        try{
            Optional<Contato> contatoOriginal = contatoRepository.findById(id);
            if (contatoOriginal.isPresent()) {
                contatoOriginal.get().setNome(contato.getNome());
                contatoOriginal.get().setEmail(contato.getEmail());
                contatoOriginal.get().setTelefone(contato.getTelefone());
                contatoRepository.save(contatoOriginal.get());
                return ResponseEntity.status(HttpStatus.OK).body("Contato atualizado com sucesso!");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Contato não encontrado para atualização!");
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @ApiOperation(value = "Deleta um contato")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try{
            contatoRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body("Contato deletado com sucesso!");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }
}
